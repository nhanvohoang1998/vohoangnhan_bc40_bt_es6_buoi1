const colorList = ["pallet", "viridian", "pewter", "cerulean", "vermillion", "lavender","celadon","saffron","fuschia","cinnabar"];

let loadingColorButton = () =>{
    let colorString = ""
    colorList.forEach((item,index)=>{
        let colorStr
        if(index == 0){
            colorStr = `<button class="color-button ${item} active" onclick="changeColorHouse(${index})"></button>`
        }else{
            colorStr = `<button class="color-button ${item}" onclick="changeColorHouse(${index})"</button>`
        }
        colorString += colorStr;
    })
    document.getElementById("colorContainer").innerHTML= colorString
}
loadingColorButton()
let changeColorHouse = (index)=>{
    document.querySelector("#colorContainer .color-button.active").classList.remove(`active`);
    document.getElementById("house").className = `house ${colorList[index]}`;
    document.querySelector(`#colorContainer .color-button.${colorList[index]}`).classList.add("active");
}
// document.getElementById("color-button").onclick = changeColorHouse