let tinhDTB = (...diem) => {
    let sum = 0
    diem.forEach((item) => {
        sum += item
    })
    return sum / diem.length
}
let tinhDTBKhoi1 = () => {
    let inpToan = Number(document.getElementById("inpToan").value);
    let inpLy = Number(document.getElementById("inpLy").value);
    let inpHoa = Number(document.getElementById("inpHoa").value);
    console.log("🚀 ~ file: main.js:12 ~ tinhDTBKhoi1 ~ inpHoa", inpHoa)
    document.getElementById("tbKhoi1").innerHTML = tinhDTB(inpToan, inpLy, inpHoa)
}

let tinhDTBKhoi2 = () => {
    let inpVan = Number(document.getElementById("inpVan").value);
    let inpSu = Number(document.getElementById("inpSu").value);
    let inpDia = Number(document.getElementById("inpDia").value);
    let inpEnglish = Number(document.getElementById("inpEnglish").value);
    document.getElementById("tbKhoi2").innerHTML =  tinhDTB(inpVan, inpSu, inpDia, inpEnglish)
}

document.getElementById("btnKhoi1").addEventListener("click", tinhDTBKhoi1);
document.getElementById("btnKhoi2").addEventListener("click", tinhDTBKhoi2);